<html>

<head>
<title>Splatalogue SLAP test</title>
</head>


<body>

<img src="http://www.cv.nrao.edu/php/splat/logo2.png">
<br><br>

<form method="get" name="searchform" action="returnSLAP.php">

<fieldset>

<legend>SLAP Test Form -> RAW DATA STREAM</legend>

<label for="slapurl"><b>SLAP Service URL:</b></label>  
  <input name="slapurl" id="slapurl" size="50" maxlength="60" 
value="http://find.nrao.edu/splata-slap/slap" /><br><br>

<label for="minwave"><b>Min wavelength:</b></label>
  <input name="minwave" id="minwave" size="10" maxlength="20" value="0.00260075" /> (meters)<br><br>

<label for="maxwave"><b>Max wavelength:</b></label>
  <input name="maxwave" id="maxwave" size="10" maxlength="20" value="0.00260080" /> (meters)<br><br>

<input type="submit" name="submission" value="Submit Query">

</fieldset>

</form>

<i>Last updated, June 2010 by B. Kent</i>


</body>

</html>
