<?
//header("Content-Type: text/xml"); 
//
// SLAPClient.php
// Created Dec. 2009. B.R. Kent, NRAO  for A. Remijan
//	Update, January 25, 2010
//	Update, June 2010 to work with Cville web server
//
//	Note from J. Malone - use CURLOPT_PROXY instead of file_get_contents
// 
// Test URLs:
//		http://voera.ncsa.uiuc.edu:8080/splat-slap/slap  //From Ray Plante, Jan. 2010
//              http://find.nrao.edu/splata-slap/slap  //A. Remijan, B. Kent, S. Witz, J. Malone

include_once "VOTable.php";
include_once "UrlUtil.php";

class SLAPClient
{
	var $urn;
	var $urlUtil;
	var $params;
	var $xml;

	//
	// constructor
	//
	function SLAPClient ($urn)
	{
		$this->urn = $urn;
		$this->urlUtil = new UrlUtil ($urn);
		
	} // Constructor
	
	//
	// Builds query string with RA/DEC/SR.
	//
	function buildQuery ($request, $wavelengthlower, $wavelengthupper, $verb=3, $keys=NULL)
	{
		$q1 = array ('REQUEST' => $request, 'WAVELENGTH' => $wavelengthlower.'/'.$wavelengthupper,
                                'VERB' => $verb);
		if ($keys != NULL)
		{
			$q1 = array_merge ($q1, $keys);
		}
		$list = array ();
		foreach ($q1 as $key => $val)
		{
			$list[] = sprintf ("%s=%s", trim($key), trim($val));
		}
		
		$q1 =  $this->urlUtil->unparseUrl (implode ('&', $list));
		return $this->urlUtil->unparseUrl (implode ('&', $list));
	} // buildQuery
		
	//
	// Returns raw VOTable in xml as string.
	//
	function getRaw ($request, $wavelengthlower, $wavelengthupper, $verb=3, $keys=NULL)
	{
		$query = $this->buildQuery ($request, $wavelengthlower, $wavelengthupper, $verb, $keys);
		//echo $query; echo '<br>';
		
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $query);
		curl_setopt($ch, CURLOPT_TIMEOUT, 180);
       		curl_setopt($ch, CURLOPT_HEADER, 0);
       		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$proxy='proxy.cv.nrao.edu:3128';
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
       		$data=curl_exec($ch);
       		// close cURL resource, and free up system resources
		curl_close($ch);
		
		return $data;
	} // getRaw

	


}; // SLAPClient Class

/******************************************************************************************/
//Edit only below this line - used for testing the SLAPClient class

//$source='http://find.nrao.edu/splata-slap/slap?REQUEST=queryData&WAVELENGTH=0.00260075/0.00260080';


//$slaptest = new SLAPClient ("http://find.nrao.edu/splata-slap/slap");

//$xmlstr=$slaptest->getRaw ('queryData', 0.00260075,0.00260080);

//echo $xmlstr;


?>
	
