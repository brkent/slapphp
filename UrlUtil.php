<?
//Modified by B. Kent, January 2010 to work with PHP 5

class UrlUtil
{
	var $url;
	var $parsedUrl;

	function UrlUtil ($inUrl)
	{
		$this->url = urldecode (html_entity_decode ($inUrl));
		$this->parsedUrl = parse_url ($this->url);
		
	} // constructor
	//
	// Converts a query string to an associative array
	//
	function qlist2array ($queryList)
	{
		$l1 = explode ('&', $queryList);
		$pairs = array ();
		foreach ($l1 as $item)
		{
			if ($item == '') 
				continue;
			$kvpair = explode ('=', $item);
			//echo "k $kvpair[0] v $kvpair[1] +\n";
			$key = $kvpair[0];
			if ($key == 'format') $key = strtoupper ($key);
			switch (count ($kvpair))
			{
				case 2:
					$pairs[$key] = $kvpair[1];
					break;
				case 1:
					$pairs[$key] = False;
					break;
				default:;
			}
		}
		return $pairs;
	} // qlist2array

	//
	// Converts associative array to query string
	//
	function array2qlist ($arr)
	{
		$buf = array ();
		foreach ($arr as $key => $value)
		{
			if ($key == '')
				continue;
			if ($value === False)
				$buf[] = $key;
			else
				$buf[] = "$key=".urlencode ($value);
		}
		return implode ('&', $buf);
	} // array2qlist

	function reassembleURL ($urlparts, $query)
	{
		$urlparts['query'] = $query;

		$url = '';
		//$list = array ('scheme','host','user','pass','path','query','fragment');

		if (@isset ($urlparts['scheme']))
		{
			$url .= $urlparts['scheme'].":";
		}
		if (@isset ($urlparts['host']))
		{
			$url .= "//".$urlparts['host'];
		}
		if (@isset ($urlparts['port']))
		{
			$url .= ':'.$urlparts['port'];
		}
		if (@isset ($urlparts['path']))
		{
			$url .= $urlparts['path'];
		}
		if (@isset ($urlparts['query']))
		{
			$url .= '?'.$urlparts['query'];
		}
		if (@isset ($urlparts['fragment']))
		{
			$url .= '#'.$urlparts['fragment'];
		}
		return $url;
	} // reassembleURL

	function unparseUrl ($queryList)
	{
		if (isset ($this->parsedUrl['query']))
		{
			$qorg = $this->parsedUrl['query'];
			$q1 = $this->qlist2array ($qorg);
			$q2 = $this->qlist2array ($queryList);
			
			$q3 = array_merge ($q1, $q2);	
			$out = $this->array2qlist ($q3);
			$query = $this->reassembleURL ($this->parsedUrl, $out);
			return $query;
		}
		$query = $this->reassembleURL ($this->parsedUrl, $queryList);
		return $query;
	} // unparseUrl

}
?>
