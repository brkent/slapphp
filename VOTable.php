<?php
//
// Class representing VOTable
// Created July 9, 2005 by Shui Hung Kwok skwok at keck.hawaii.edu
// 

class VOTable 
{
	var $dataSrc;
	var $srcType; // 1=file, 2=string
	var $root;

	//
	// Constructor
	//
	function VOTable ($src='')
	{
		
		$this->root = False;
		if ($src != '')
		{
			$this->read ($src);
		}
	} // Constructor
	
	//
	// root contains the votable as php typed variables.
	//
	function getData ()
	{
		return $this->root;
	} // getData

	// 
	// src is file name or URL
	// Reads VOTable from src.
	//
	function read ($src)
	{
		$this->dataSrc = $src;
		$this->srcType = 1;
		$this->root = @simplexml_load_file ($src);
	} // read

	//
	// Parses a VOTable as a string.
	//
	function readFromString ($string)
	{
		$this->srcType = 2;
		$this->root = @simplexml_load_string ($string);
	} // readFromString

	//
	// Outputs the VOTable content as XML.
	//
	function getAsXML ()
	{
		return $this->root->asXML ();
	} // getAsXML

	//
	// Returns a list of attributes of fields.
	// attributes are stored as associative arrays, name=>value pairs.
	//
	function getFields ($withDescription=0)
	{
		//$flds = $this->root->xpath ("//FIELD");
		@$flds = $this->root->RESOURCE->TABLE->FIELD;
		if ($flds === NULL) return False;
		$list = array ();
		foreach ($flds as $fld)
		{
			$attr = $fld->attributes ();
			if ($withDescription && isset ($fld->DESCRIPTION))
			{
				$attr['DESCRIPTION'] = (string) $fld->DESCRIPTION;
			}
			$list[] = $attr;
		} // for
		return $list;
	} // getFields

	//
	// Finds the position of a field given its name.
	// The position is column index in the table.
	//
	function findIdx ($attrName)
	{
		// Find index for attribute attrName
		$flds = $this->getFields ();
		foreach ($flds as $fld)
		{
			$attrs = $fld->attributes ();
			$idx = 0;
			foreach ($attrs as $name => $val)
			{
				if ($name == $attrName)
				{
					return $idx;
				}
				++$idx;
			}
		}
		throw new Exception ("$attrName not found");
	} // findIdx

	//
	// Returns table data as array
	//
	function getTableData ()
	{
		//$tableRows = $this->root->xpath ("/RESOURCE/TABLE/DATA/TABLEDATA/TR/");
		@$tableRows = $this->root->RESOURCE->TABLE->DATA->TABLEDATA->TR;
		return $tableRows;
	} // getTableData

	//
	// Scans the table and output a CSV line per row via myWrite
	//
	function output2CSV ($myWrite = '__CSVWriter')
	{
		$fields = $this->getFields ();
		$attrName = 'name';

		$list = array ('name', 'id', 'ID');
		$found = 0;
		foreach ($list as $i)
		{
			if ($fields != NULL) foreach ($fields as $fd)
			{
				if (isset ($fd[$i]))
				{
					$attrName = $i;
					$found = 1;
					break;
				}
			}
			if ($found) break;
		}
		$buf = array ();
		if ($fields != NULL) foreach ($fields as $fd)
		{
			$str = $fd[$attrName];
			if (strpos ($str, ',') === False)
				$buf[] = $str;
			else
				$buf[] = '"'.$str.'"';
		}
		$myWrite (implode (',', $buf));

		$tableRows = $this->getTableData ();
		if (isset ($tableRows[0])) foreach ($tableRows as $row)
		{
			$buf = array ();
			foreach ($row->TD as $col)
			{
				$buf[] = trim ((string) $col);
			}
			$myWrite ( implode (',', $buf));
		}
	} // output2CSV
}

function __CSVWriter ($str)
{
	echo $str;
	echo "\n";
} // __CSVWriter

?>
